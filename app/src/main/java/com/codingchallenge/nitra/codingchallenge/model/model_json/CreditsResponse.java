package com.codingchallenge.nitra.codingchallenge.model.model_json;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class CreditsResponse {
    @SerializedName("id")
    private Integer id;
    @SerializedName("cast")
    private List<Cast> cast;


    public CreditsResponse() {

    }

    public CreditsResponse(List<Cast> cast) {
        this.cast = cast;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Cast> getCast() {
        return cast;
    }

    public void setCast(List<Cast> cast) {
        this.cast = cast;
    }
}
