package com.codingchallenge.nitra.codingchallenge.controller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codingchallenge.nitra.codingchallenge.R;
import com.codingchallenge.nitra.codingchallenge.model.helper.ListGenres;
import com.codingchallenge.nitra.codingchallenge.model.helper.picture;
import com.codingchallenge.nitra.codingchallenge.model.model_json.Movie;

import java.util.ArrayList;
import java.util.List;


public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieViewHolder> {

    @NonNull
    private List<List<Movie>> genres_movies = new ArrayList<>();
    @NonNull
    private List<Integer> generesInt = new ArrayList<>();
    @NonNull
    private List<String> generesTitle = new ArrayList<>();
    private boolean categorizate = false;
    private List<Movie> movies;
    private int rowLayout;
    private Context context;
    private picture picture_picasso;

    private inflate_items_movie inflateItems;
    private int lastPosition = -1;
    private ListGenres listGenres;


    public MoviesAdapter(@NonNull List<Movie> movies, int rowLayout, Context context, picture picture_picasso, boolean categorizate, ListGenres listGenres) {
        this.listGenres = listGenres;
        this.categorizate = categorizate;
        this.movies = movies;
        this.rowLayout = rowLayout;
        this.context = context;
        this.picture_picasso = picture_picasso;
        this.inflateItems = new inflate_items_movie(context, picture_picasso);
        loadCategorizate();

        if (movies.size() == 0) IfNoMovie();
    }

    public MoviesAdapter(@NonNull Movie movie, int rowLayout, Context context, picture picture_picasso, boolean categorizate, ListGenres listGenres) {
        this.listGenres = listGenres;
        this.categorizate = categorizate;
        this.movies = new ArrayList<>();
        this.movies.add(movie);
        this.rowLayout = rowLayout;
        this.context = context;
        this.picture_picasso = picture_picasso;
        this.inflateItems = new inflate_items_movie(context, picture_picasso);
        loadCategorizate();
        if (movies.size() == 0) IfNoMovie();
    }

    private void loadCategorizate() {
        if (categorizate) {
            setCategorizate();
        }
    }

    private void setCategorizate() {
        generesInt.clear();
        genres_movies.clear();
        generesTitle.clear();

        for (Movie movie : movies) {
            int id_Gere_Principal;
            if (movie.getGenreIds() != null) {
                if (movie.getGenreIds().size() != 0) {
                    id_Gere_Principal = movie.getGenreIds().get(0);
                } else {
                    id_Gere_Principal = 0;
                }

                int id_index = generesInt.indexOf(id_Gere_Principal);

                if (id_index < 0) {
                    generesInt.add(id_Gere_Principal);
                    generesTitle.add(listGenres.getGenreName(id_Gere_Principal));
                    genres_movies.add(new ArrayList<Movie>());
                }

                id_index = generesInt.indexOf(id_Gere_Principal);
                genres_movies.get(id_index).add(movie);
            }
        }
    }

    public void setSearchResponse(List<Movie> movieList) {
        this.movies = movieList;
        loadCategorizate();
        notifyDataSetChanged();
    }

    public void setSearchResponse(Movie movie) {
        this.movies.clear();
        this.movies.add(movie);
        loadCategorizate();
        notifyDataSetChanged();
    }

    private void IfNoMovie() {
        Movie movie = new Movie();

        movie.setTitle(context.getString(R.string.noData));
        movie.setOverview(context.getString(R.string.verifyConnection));
        movie.setOffline(true);
        movie.setNoData(true);
        movies.add(movie);
    }


    @NonNull
    @Override
    public MoviesAdapter.MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, final int position) {

        if (categorizate) {
            inflateItems.inflate_movie(holder.itemView, movies.get(position), genres_movies.get(position), generesTitle.get(position));
        } else {
            inflateItems.inflate_movie(holder.itemView, movies.get(position), null, null);
        }

        lastPosition = animViews.setAnimation(holder.itemView, position, lastPosition);
    }

    @Override
    public int getItemCount() {
        int size;
        if (categorizate) {
            size = genres_movies.size();
        } else {
            if (movies == null) {
                size = 0;
            } else {
                size = movies.size();
            }
        }
        return size;
    }

    static class MovieViewHolder extends RecyclerView.ViewHolder {

        MovieViewHolder(@NonNull View v) {
            super(v);
        }
    }
}