package com.codingchallenge.nitra.codingchallenge.model.callback;


import android.support.annotation.NonNull;

import com.codingchallenge.nitra.codingchallenge.model.model_json.CreditsResponse;
import com.codingchallenge.nitra.codingchallenge.model.model_json.GenreResponse;
import com.codingchallenge.nitra.codingchallenge.model.model_json.Movie;
import com.codingchallenge.nitra.codingchallenge.model.model_json.MoviesResponse;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;


public interface MovieApis {
    /**
	 *
	 * @param apiKey
	 * @param language
	 * @param page
	 */
	@NonNull
	@GET("movie/top_rated")
    Observable<MoviesResponse> getTopRatedMovies(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") int page);

    /**
	 *
	 * @param apiKey
	 * @param language
	 * @param page
	 */
	@NonNull
	@GET("movie/popular")
    Observable<MoviesResponse> getPopularMovies(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") int page);

    /**
	 *
	 * @param apiKey
	 * @param language
	 * @param page
	 */
	@NonNull
	@GET("movie/upcoming")
    Observable<MoviesResponse> getUpcomingMovies(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") int page);

    /**
	 *
	 * @param id
	 * @param apiKey
	 * @param language
	 */
	@NonNull
	@GET("movie/{id}")
    Observable<Movie> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey, @Query("language") String language);

    /**
	 *
	 * @param movie_id
	 * @param apiKey
	 */
	@NonNull
	@GET("movie/{movie_id}/credits")
    Observable<CreditsResponse> getMovieCredits(@Path("movie_id") int movie_id, @Query("api_key") String apiKey);

    /**
	 *
	 * @param apiKey
	 * @param language
	 */
	@NonNull
	@GET("genre/movie/list")
    Observable<GenreResponse> getMovieGenres(@Query("api_key") String apiKey, @Query("language") String language);

    /**
	 *
	 * @param apiKey
	 * @param language
	 */
	@NonNull
	@GET("genre/tv/list")
    Observable<GenreResponse> getTvGenres(@Query("api_key") String apiKey, @Query("language") String language);

    /**
	 *
	 * @param query
	 * @param apiKey
	 * @param language
	 * @param page
	 */
	@NonNull
	@GET("search/movie")
    Observable<MoviesResponse> getMovieSearch(@Query("query") String query, @Query("api_key") String apiKey, @Query("language") String language, @Query("page") int page);

}
