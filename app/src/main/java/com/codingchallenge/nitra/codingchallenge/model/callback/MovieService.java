package com.codingchallenge.nitra.codingchallenge.model.callback;

import com.codingchallenge.nitra.codingchallenge.model.model_json.CreditsResponse;
import com.codingchallenge.nitra.codingchallenge.model.model_json.GenreResponse;
import com.codingchallenge.nitra.codingchallenge.model.model_json.Movie;
import com.codingchallenge.nitra.codingchallenge.model.model_json.MoviesResponse;

import rx.Observable;



public interface MovieService {

    Observable<MoviesResponse> getTopRatedMovies(int page);

    Observable<MoviesResponse> getPopularMovies(int page);

    Observable<MoviesResponse> getUpcomingMovies(int page);

    Observable<Movie> getMovieDetails(int id);

    Observable<CreditsResponse> getMovieCredits(int movie_id);

    //Observable<GenreResponse> getMovieGenres(GenresFetchListener genresFetchListener);
    void getMovieGenres();

    Observable<GenreResponse> getTvGenres();

    Observable<MoviesResponse> getMovieSearch(int page, String query);
}
