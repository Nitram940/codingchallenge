package com.codingchallenge.nitra.codingchallenge.model.callback;

import com.codingchallenge.nitra.codingchallenge.model.model_json.Cast;
import com.codingchallenge.nitra.codingchallenge.model.model_json.Movie;

import java.util.List;


public interface MovieFetchListener {
    void onDeliverAllMovie(List<Movie> movieItems, int pages);

    void onDeliverAllCast(List<Cast> CastItems);

    void onDeliverMovie(Movie movie_item);

    void onHideDialog();
}
