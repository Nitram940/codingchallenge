package com.codingchallenge.nitra.codingchallenge.di;

import com.codingchallenge.nitra.codingchallenge.controller.ApiRetrofit;
import com.codingchallenge.nitra.codingchallenge.controller.adapter.inflate_items_cast;
import com.codingchallenge.nitra.codingchallenge.model.database.MoviesDatabase;
import com.codingchallenge.nitra.codingchallenge.model.helper.BusProvider;
import com.codingchallenge.nitra.codingchallenge.model.helper.ListGenres;
import com.codingchallenge.nitra.codingchallenge.model.helper.Utils;
import com.codingchallenge.nitra.codingchallenge.model.helper.picture;
import com.codingchallenge.nitra.codingchallenge.ui.FragmentBaseMovie;
import com.codingchallenge.nitra.codingchallenge.ui.FullscreenActivityDetailMovie;
import com.codingchallenge.nitra.codingchallenge.ui.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by nitra on 24/12/2017.
 */

@Singleton
@Component(modules={AppModule.class, MovieModule.class, ApiRetrofit.class, picture.class, MoviesDatabase.class, inflate_items_cast.class, BusProvider.class, ListGenres.class, Utils.class})
public interface AppComponent {

    void inject(MainActivity activity);

    void inject(FragmentBaseMovie fragmentBaseMovie);

    void inject(FullscreenActivityDetailMovie fullscreenActivityDetailMovie);
}