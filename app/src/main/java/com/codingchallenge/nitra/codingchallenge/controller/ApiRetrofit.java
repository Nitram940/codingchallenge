package com.codingchallenge.nitra.codingchallenge.controller;

import com.codingchallenge.nitra.codingchallenge.model.callback.MovieApis;
import com.codingchallenge.nitra.codingchallenge.model.helper.Constants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiRetrofit {

    @Provides
    @Singleton
    MovieApis movieApis() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.HTTP.BASE_URL)
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        return retrofit.create(MovieApis.class);
    }


}
