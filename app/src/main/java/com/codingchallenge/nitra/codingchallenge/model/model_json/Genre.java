package com.codingchallenge.nitra.codingchallenge.model.model_json;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;


public class Genre {
    @SerializedName("id")
    private Integer id;
    @SerializedName("name")
    private String name;

    @NonNull
    private Boolean offline = false;
    @NonNull
    private Boolean detail = false;

    public Genre() {

    }

    public Genre(Integer id, String name) {
        this.id = id;
        this.name = name;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
