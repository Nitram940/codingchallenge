package com.codingchallenge.nitra.codingchallenge.model.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.codingchallenge.nitra.codingchallenge.model.callback.MovieApisImpl;

import java.util.Locale;
import java.util.Objects;

public class LocaleChangedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Objects.equals(intent.getAction(), Intent.ACTION_LOCALE_CHANGED)) {
            MovieApisImpl.LANGUAGE_API= Locale.getDefault().toLanguageTag();
            Log.v("LocaleChangedRecevier", Locale.getDefault().toLanguageTag());
        }

    }
}
