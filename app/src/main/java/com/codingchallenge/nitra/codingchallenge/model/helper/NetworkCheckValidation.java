package com.codingchallenge.nitra.codingchallenge.model.helper;

public interface NetworkCheckValidation {
    void onNetworkCheckValidation(Boolean bool);
}
