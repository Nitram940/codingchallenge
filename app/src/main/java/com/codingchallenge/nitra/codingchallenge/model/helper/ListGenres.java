package com.codingchallenge.nitra.codingchallenge.model.helper;

import android.support.annotation.NonNull;

import com.codingchallenge.nitra.codingchallenge.model.model_json.Genre;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;

@Singleton
@Module
public class ListGenres {

    private List<String> genreName=new ArrayList<>();
    private List<Integer> genreId=new ArrayList<>();

    @Inject
    ListGenres() {

    }

    public void setGenreNameId(@NonNull List<Genre> genreList) {
        genreList.add(new Genre(0,"Sin Categoria"));
        for (Genre genre : genreList) {
            if (genreId.indexOf(genre.getId()) < 0) {
                genreName.add(genre.getName());
                genreId.add(genre.getId());
            }
        }
    }

    public String getGenreName(int id_genre) {
        int index = genreId.indexOf(id_genre);
        if (index >= 0) {
            return genreName.get(index);
        } else {
            return "";
        }
    }


    public List<String> getGenreName() {
        return genreName;
    }

    public void setGenreName(List<String> genreName) {
        this.genreName = genreName;
    }

    public List<Integer> getGenreId() {
        return genreId;
    }

    public void setGenreId(List<Integer> genreId) {
        this.genreId = genreId;
    }
}
