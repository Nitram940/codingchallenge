package com.codingchallenge.nitra.codingchallenge.model.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;

import com.codingchallenge.nitra.codingchallenge.model.callback.MovieFetchListener;
import com.codingchallenge.nitra.codingchallenge.model.helper.Constants;
import com.codingchallenge.nitra.codingchallenge.model.helper.ListGenres;
import com.codingchallenge.nitra.codingchallenge.model.model_json.Cast;
import com.codingchallenge.nitra.codingchallenge.model.model_json.Genre;
import com.codingchallenge.nitra.codingchallenge.model.model_json.Movie;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;

@Singleton
@Module
public class MoviesDatabase extends SQLiteOpenHelper {

    private static final String TAG = MoviesDatabase.class.getSimpleName();

    private ListGenres listGenres;

    @Inject
    public MoviesDatabase(Context context,ListGenres listGenres) {
        super(context, Constants.DATABASE.DB_NAME, null, Constants.DATABASE.DB_VERSION);
        this.listGenres=listGenres;
    }

    @Override
    public void onCreate(@NonNull SQLiteDatabase db) {
        try {
            db.execSQL(Constants.DATABASE.CREATE_TABLE_MOVIE);
            db.execSQL(Constants.DATABASE.CREATE_TABLE_GENRE);
            db.execSQL(Constants.DATABASE.CREATE_TABLE_GENRE_MOVIE);
            db.execSQL(Constants.DATABASE.CREATE_TABLE_CAST);
        } catch (SQLException ex) {
            Log.d(TAG, ex.getMessage());
        }
    }

    @Override
    public void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Constants.DATABASE.DROP_TABLE_MOVIE);
        db.execSQL(Constants.DATABASE.DROP_TABLE_GENRE);
        db.execSQL(Constants.DATABASE.DROP_TABLE_GENRE_MOVIE);
        db.execSQL(Constants.DATABASE.DROP_TABLE_CAST);
        this.onCreate(db);
    }

    public void addListMovie(@NonNull List<Movie> movieList) {
        for (Movie movie : movieList) {
            addMovie(movie);
        }
    }

    public void addListGenre(@NonNull List<Genre> genreList) {
        for (Genre genre : genreList) {
            addGenre(genre);
        }
    }

    private void addGenre(@NonNull Genre genre) {

        //Log.d(TAG, "Genres Got " + genre.getName());

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Constants.DATABASE.ID_GENRE, genre.getId());
        values.put(Constants.DATABASE.NAME, genre.getName());

        try {
            db.replaceOrThrow(Constants.DATABASE.TABLE_GENRE, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
    }

    public void addListCasts(@NonNull List<Cast> castList, int id_movie) {
        for (Cast cast : castList) {
            addCast(cast, id_movie);
        }
    }

    private void addCast(@NonNull Cast cast, int id_movie) {

        //Log.d(TAG, "Cast Got " + genre.getName());

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Constants.DATABASE.ID_CAST, cast.getId());
        values.put(Constants.DATABASE.CAST_ID, cast.getCast_id());
        values.put(Constants.DATABASE.CHARACTER, cast.getCharacter());
        values.put(Constants.DATABASE.CREDIT_ID, cast.getCredit_id());
        values.put(Constants.DATABASE.GENDER, cast.getGender());
        values.put(Constants.DATABASE.NAME, cast.getName());
        values.put(Constants.DATABASE.ORDER, cast.getOrder());
        values.put(Constants.DATABASE.ID_MOVIE, id_movie);
        values.put(Constants.DATABASE.PROFILE_PATH, cast.getProfile_path());

        try {
            db.replaceOrThrow(Constants.DATABASE.TABLE_CAST, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
    }


    public void addMovie(@NonNull Movie Movie) {

        //Log.d(TAG, "Values Got " + Movie.getTitle());

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        ContentValues valuesGenresMovie = new ContentValues();

        values.put(Constants.DATABASE.ID_MOVIE, Movie.getId());
        values.put(Constants.DATABASE.ADULT, Movie.isAdult());
        values.put(Constants.DATABASE.BACKDROPPATH, Movie.getBackdropPath());
        values.put(Constants.DATABASE.POSTERPATH, Movie.getPosterPath());
        values.put(Constants.DATABASE.OVERVIEW, Movie.getOverview());
        values.put(Constants.DATABASE.RELEASEDATE, Movie.getReleaseDate());
        values.put(Constants.DATABASE.ORIGINALTITLE, Movie.getOriginalTitle());
        values.put(Constants.DATABASE.ORIGINALLANGUAGE, Movie.getOriginalLanguage());
        values.put(Constants.DATABASE.TITLE, Movie.getTitle());
        values.put(Constants.DATABASE.POPULARITY, Movie.getPopularity());
        values.put(Constants.DATABASE.VOTECOUNT, Movie.getVoteCount());
        values.put(Constants.DATABASE.VIDEO, Movie.getVideo());
        values.put(Constants.DATABASE.VOTEAVERAGE, Movie.getVoteAverage());
        values.put(Constants.DATABASE.TAGLINE, Movie.getTagline());
        values.put(Constants.DATABASE.STATUS, Movie.getStatus());

        if (Movie.getGenreIds() != null) {
            for (Integer genreId : Movie.getGenreIds()) {
                valuesGenresMovie.put(Constants.DATABASE.ID_GENRE, genreId);
                valuesGenresMovie.put(Constants.DATABASE.ID_MOVIE, Movie.getId());
                try {
                    db.replaceOrThrow(Constants.DATABASE.TABLE_GENRE_MOVIE, null, valuesGenresMovie);
                } catch (Exception e) {
                    Log.e(TAG, "ERR MOVIE_GENRE " + e.getMessage());
                }
            }
        }


        try {
            db.replaceOrThrow(Constants.DATABASE.TABLE_MOVIE, null, values);
        } catch (Exception e) {
            Log.e(TAG, "ERR MOVIE" + e.getMessage());
        }

        db.close();
    }

    public void fetchMovie(MovieFetchListener listener, int query, int pageQuery, String query_s) {
        MovieFetcher fetcher = new MovieFetcher(listener, this.getWritableDatabase(), query, pageQuery, query_s);
        fetcher.start();
    }

    public void fetchMovie(MovieFetchListener listener, int idMovie) {
        MovieFetcher fetcher = new MovieFetcher(listener, this.getWritableDatabase(), idMovie);
        fetcher.start();
    }

    public void fetchGenres() {
        GenresFetcher fetcher = new GenresFetcher(this.getWritableDatabase(), true);
        fetcher.start();
    }

    public class MovieFetcher extends Thread {

        private final MovieFetchListener mListener;
        private final SQLiteDatabase mDb;
        private int query;
        private String query_s = "";
        private int pageQuery;
        private int pageSize = 0;
        private int idMovie = 0;
        private boolean genres = false;


        MovieFetcher(MovieFetchListener listener, SQLiteDatabase db, int query, int pageQuery, String query_s) {
            this.query = query;
            this.pageQuery = pageQuery;
            this.query_s = query_s;
            mListener = listener;
            mDb = db;
        }

        MovieFetcher(MovieFetchListener listener, SQLiteDatabase db, int idMovie) {
            this.query = 9;
            mListener = listener;
            mDb = db;
            this.idMovie = idMovie;
        }

        public MovieFetcher(MovieFetchListener listener, SQLiteDatabase db, boolean genres) {
            this.genres = genres;
            mListener = listener;
            mDb = db;
        }

        @NonNull
        List<Integer> getGenresId(int idMovie) {
            Cursor cursor_genresID = mDb.rawQuery(Constants.DATABASE.GET_GENRESID_MOVIE(idMovie), null);

            List<Integer> genresId = new ArrayList<>();

            if (cursor_genresID.getCount() > 0) {

                if (cursor_genresID.moveToFirst()) {
                    do {
                        genresId.add(cursor_genresID.getInt(cursor_genresID.getColumnIndex(Constants.DATABASE.ID_GENRE)));
                    } while (cursor_genresID.moveToNext());
                }
                cursor_genresID.close();
            }

            return genresId;
        }


        @Override
        public void run() {
            Cursor cursor;
            Cursor cursor_cast = null;
            Cursor cursorSize;
            String pagination = "";

            if (idMovie == 0) {

                if (query != 3) {
                    cursorSize = mDb.rawQuery(Constants.DATABASE.GET_ALL_MOVIES_COUNT, null);
                } else {
                    cursorSize = mDb.rawQuery(Constants.DATABASE.GET_SEARCH_MOVIES_COUNT(query_s), null);
                }

                cursorSize.moveToFirst();
                pageSize = (int) Math.ceil((double) cursorSize.getInt(0) / 20);

                if (pageQuery > pageSize) {
                    pageQuery = 0;
                }

                pagination = " LIMIT " + 20 + " OFFSET  " + ((pageQuery) * 20);
                cursorSize.close();
            }

            switch (query) {
                case 0:
                    cursor = mDb.rawQuery(Constants.DATABASE.GET_POPULAR_MOVIES + pagination, null);
                    break;
                case 1:
                    cursor = mDb.rawQuery(Constants.DATABASE.GET_QUERY_TOP_RATED + pagination, null);
                    break;
                case 2:
                    cursor = mDb.rawQuery(Constants.DATABASE.GET_UPCOMING_MOVIES + pagination, null);
                    break;
                case 3:
                    cursor = mDb.rawQuery(Constants.DATABASE.GET_SEARCH_MOVIES(query_s) + pagination, null);
                    break;
                case 9:
                    cursor = mDb.rawQuery(Constants.DATABASE.GET_MOVIE_DETAIL(idMovie), null);
                    cursor_cast = mDb.rawQuery(Constants.DATABASE.GET_ALL_CAST(idMovie), null);
                    break;
                default:
                    cursor = mDb.rawQuery(Constants.DATABASE.GET_ALL_MOVIES, null);
                    break;
            }

            final List<Movie> movieList = new ArrayList<>();

            final List<Cast> castList = new ArrayList<>();

            if (cursor.getCount() > 0) {

                if (cursor.moveToFirst()) {
                    do {
                        Movie Movie = new Movie();
                        Movie.setOffline(true);
                        Movie.setPosterPath(cursor.getString(cursor.getColumnIndex(Constants.DATABASE.POSTERPATH)));
                        Movie.setAdult(cursor.getInt(cursor.getColumnIndex(Constants.DATABASE.ADULT)) > 0);
                        Movie.setOverview(cursor.getString(cursor.getColumnIndex(Constants.DATABASE.OVERVIEW)));
                        Movie.setReleaseDate(cursor.getString(cursor.getColumnIndex(Constants.DATABASE.RELEASEDATE)));
                        Movie.setId(cursor.getInt(cursor.getColumnIndex(Constants.DATABASE.ID_MOVIE)));
                        Movie.setOriginalTitle(cursor.getString(cursor.getColumnIndex(Constants.DATABASE.ORIGINALTITLE)));
                        Movie.setOriginalLanguage(cursor.getString(cursor.getColumnIndex(Constants.DATABASE.ORIGINALLANGUAGE)));
                        Movie.setTitle(cursor.getString(cursor.getColumnIndex(Constants.DATABASE.TITLE)));
                        Movie.setBackdropPath(cursor.getString(cursor.getColumnIndex(Constants.DATABASE.BACKDROPPATH)));
                        Movie.setPopularity(cursor.getDouble(cursor.getColumnIndex(Constants.DATABASE.POPULARITY)));
                        Movie.setVoteCount(cursor.getInt(cursor.getColumnIndex(Constants.DATABASE.VOTECOUNT)));
                        Movie.setVideo(cursor.getInt(cursor.getColumnIndex(Constants.DATABASE.VIDEO)) > 0);
                        Movie.setVoteAverage(cursor.getDouble(cursor.getColumnIndex(Constants.DATABASE.VOTEAVERAGE)));
                        Movie.setTagline(cursor.getString(cursor.getColumnIndex(Constants.DATABASE.TAGLINE)));
                        Movie.setStatus(cursor.getString(cursor.getColumnIndex(Constants.DATABASE.STATUS)));
                        Movie.setGenreIds(getGenresId(Movie.getId()));

                        if (query == 9) {
                            Movie.setGenreTxt(cursor.getString(cursor.getColumnIndex(Constants.DATABASE.NAME)));
                        }

                        movieList.add(Movie);
                        publishMovie(Movie);

                    } while (cursor.moveToNext());
                }
            }

            if (cursor_cast != null) {

                if (cursor_cast.getCount() > 0) {

                    if (cursor_cast.moveToFirst()) {
                        do {
                            Cast cast = new Cast();
                            cast.setId(cursor_cast.getInt(cursor_cast.getColumnIndex(Constants.DATABASE.ID_CAST)));
                            cast.setCast_id(cursor_cast.getInt(cursor_cast.getColumnIndex(Constants.DATABASE.CAST_ID)));
                            cast.setCharacter(cursor_cast.getString(cursor_cast.getColumnIndex(Constants.DATABASE.CHARACTER)));
                            cast.setCredit_id(cursor_cast.getString(cursor_cast.getColumnIndex(Constants.DATABASE.CREDIT_ID)));
                            cast.setGender(cursor_cast.getInt(cursor_cast.getColumnIndex(Constants.DATABASE.GENDER)));
                            cast.setName(cursor_cast.getString(cursor_cast.getColumnIndex(Constants.DATABASE.NAME)));
                            cast.setOrder(cursor_cast.getInt(cursor_cast.getColumnIndex(Constants.DATABASE.ORDER)));
                            cast.setId_movie(cursor_cast.getInt(cursor_cast.getColumnIndex(Constants.DATABASE.ID_MOVIE)));
                            cast.setProfile_path(cursor_cast.getString(cursor_cast.getColumnIndex(Constants.DATABASE.PROFILE_PATH)));

                            castList.add(cast);
                        } while (cursor_cast.moveToNext());
                    }
                    cursor_cast.close();
                }
            }

            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    mListener.onDeliverAllMovie(movieList, pageSize);

                    if (castList.size() > 0) {
                        mListener.onDeliverAllCast(castList);
                    }

                    mListener.onHideDialog();
                }
            });


            cursor.close();
        }

        void publishMovie(final Movie movieItem) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    mListener.onDeliverMovie(movieItem);
                }
            });
        }
    }


    public class GenresFetcher extends Thread {

        private final SQLiteDatabase mDb;
        private boolean genres = false;

        GenresFetcher(SQLiteDatabase db, boolean genres) {
            this.genres = genres;
            mDb = db;
        }

        @Override
        public void run() {
            Cursor cursor;
            cursor = mDb.rawQuery(Constants.DATABASE.GET_ALL_GENRES, null);
            final List<Genre> genreList = new ArrayList<>();


            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        Genre genre = new Genre();

                        genre.setId(cursor.getInt(cursor.getColumnIndex(Constants.DATABASE.ID_GENRE)));
                        genre.setName(cursor.getString(cursor.getColumnIndex(Constants.DATABASE.NAME)));

                        genreList.add(genre);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();


            listGenres.setGenreNameId(genreList);
        }
    }


}
