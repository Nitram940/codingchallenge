package com.codingchallenge.nitra.codingchallenge.model.helper;

import android.support.annotation.NonNull;

public class Constants {

    @NonNull
    public static String API_KEY = "3549c5c8ed9ae11071d5ffbab10d8047";

    public static final class HTTP {
        public static final String BASE_URL = "http://api.themoviedb.org/3/";
        public static final String BASE_URL_IMAGE_W500 = "http://image.tmdb.org/t/p/w500";
        public static final String BASE_URL_IMAGE_W300 = "http://image.tmdb.org/t/p/w300";
        public static final String BASE_URL_IMAGE_W92 = "http://image.tmdb.org/t/p/w92";
    }

    public static final class DATABASE {

        public static final String DB_NAME = "movies";
        public static final int DB_VERSION = 6;
        public static final String TABLE_MOVIE = "movie";
        public static final String TABLE_GENRE = "genre";
        public static final String TABLE_GENRE_MOVIE = "genre_movie";
        public static final String TABLE_CAST = "cast";

        //CAST ITEMS
        @NonNull
        public static String ID_CAST = "ID_CAST";
        @NonNull
        public static String CAST_ID = "CAST_ID";
        @NonNull
        public static String CHARACTER = "CHARACTER";
        @NonNull
        public static String CREDIT_ID = "CREDIT_ID";
        @NonNull
        public static String GENDER = "GENDER";
        @NonNull
        public static String ORDER = "ORDER_CAST";
        @NonNull
        public static String PROFILE_PATH = "PROFILE_PATH";


        //GENRE ITEMS
        public static final String NAME = "NAME"; //STRING
        public static final String ID_GENRE = "ID_GENRE"; //STRING

        //movie items
        public static final String POSTERPATH = "POSTER_PATH"; //STRING
        public static final String ADULT = "ADULT"; //BOOLEAN
        public static final String OVERVIEW = "OVERVIEW"; //STRING
        public static final String RELEASEDATE = "RELEASE_DATE"; //STRING
        public static final String GENREIDS = "GENRE_IDS"; //LIST INTEGER
        public static final String ID_MOVIE = "ID_MOVIE"; //INTEGER
        public static final String ORIGINALTITLE = "ORIGINAL_TITLE"; //STRING
        public static final String ORIGINALLANGUAGE = "ORIGINAL_LANGUAGE"; //STRING
        public static final String TITLE = "TITLE"; //STRING
        public static final String BACKDROPPATH = "BACKDROP_PATH"; //STRING
        public static final String POPULARITY = "POPULARITY"; //DOBLE
        public static final String VOTECOUNT = "VOTE_COUNT";  //INTEGER
        public static final String VIDEO = "VIDEO";  //BOOLEAN
        public static final String VOTEAVERAGE = "VOTE_AVERAGE"; //DOUBLE

        //DETAIL ITEMS
        public static final String TAGLINE = "TAGLINE"; //STRING
        public static final String STATUS = "STATUS"; //STRING

        public static final String DROP_TABLE_MOVIE = "DROP TABLE IF EXISTS " + TABLE_MOVIE;
        public static final String DROP_TABLE_GENRE = "DROP TABLE IF EXISTS " + TABLE_GENRE;
        public static final String DROP_TABLE_GENRE_MOVIE = "DROP TABLE IF EXISTS " + TABLE_GENRE_MOVIE;
        public static final String DROP_TABLE_CAST = "DROP TABLE IF EXISTS " + TABLE_CAST;

        public static final String GET_QUERY_TOP_RATED = "SELECT * FROM " + TABLE_MOVIE + " ORDER BY " + VOTEAVERAGE + " DESC";
        public static final String GET_POPULAR_MOVIES = "SELECT * FROM " + TABLE_MOVIE + " ORDER BY " + POPULARITY + " DESC";
        public static final String GET_UPCOMING_MOVIES = "SELECT * FROM " + TABLE_MOVIE + " ORDER BY " + RELEASEDATE + " DESC";

        public static String GET_SEARCH_MOVIES(String query) {
            return "SELECT * FROM " +
                    TABLE_MOVIE + " " +
                    "WHERE " + TITLE + " LIKE '%" + query +
                    "%' OR " + ORIGINALTITLE + " LIKE '%" + query
                    + "%' OR " + OVERVIEW + " LIKE '%" + query + "%'";
        }

        public static String GET_SEARCH_MOVIES_COUNT(String query) {
            return "SELECT COUNT(*) AS CONTEO FROM " +
                    TABLE_MOVIE + " " +
                    "WHERE " + TITLE + " LIKE '%" + query +
                    "%' OR " + ORIGINALTITLE + " LIKE '%" + query
                    + "%' OR " + OVERVIEW + " LIKE '%" + query + "%'";
        }

        public static final String GET_ALL_MOVIES = "SELECT * FROM " + TABLE_MOVIE;
        public static final String GET_ALL_MOVIES_COUNT = "SELECT COUNT(*) AS CONTEO FROM " + TABLE_MOVIE;

        public static String GET_ALL_CAST(int id) {
            return "SELECT * FROM " + TABLE_CAST + " WHERE " + ID_MOVIE + "=" + id + " ORDER BY " + ORDER + " ASC";
        }

        public static String GET_GENRESID_MOVIE(int id) {
            return "SELECT * FROM " + TABLE_GENRE_MOVIE + "  WHERE " +
                    ID_MOVIE + "=" + id ;
        }

        @NonNull
        public static String GET_ALL_GENRES = "SELECT * FROM " + TABLE_GENRE ;


        public static String GET_MOVIE_DETAIL(int id) {
            return "SELECT A.*, group_concat(C." + NAME + ") AS " + NAME + " FROM " + TABLE_MOVIE + " AS A " +
                    "LEFT JOIN " + TABLE_GENRE_MOVIE + " AS B ON (B." + ID_MOVIE + "=A." + ID_MOVIE + ")" +
                    "LEFT JOIN " + TABLE_GENRE + " AS C  ON (C." + ID_GENRE + "=B." + ID_GENRE + ")  WHERE A." +
                    ID_MOVIE + "=" + id + " GROUP BY A." + ID_MOVIE;
        }

        public static final String CREATE_TABLE_CAST = "CREATE TABLE IF NOT EXISTS " + TABLE_CAST + "" +
                "(" + ID_CAST + " INTEGER PRIMARY KEY not null," +
                CAST_ID + " INTEGER not null," +
                CHARACTER + " TEXT not null," +
                CREDIT_ID + " TEXT not null," +
                GENDER + " INTEGER," +
                NAME + " TEXT not null," +
                ORDER + " INTEGER not null," +
                ID_MOVIE + " INTEGER not null," +
                PROFILE_PATH + " TEXT)";


        public static final String CREATE_TABLE_MOVIE = "CREATE TABLE IF NOT EXISTS " + TABLE_MOVIE + "" +
                "(" + ID_MOVIE + " INTEGER PRIMARY KEY not null," +
                POSTERPATH + " TEXT," +
                OVERVIEW + " TEXT," +
                RELEASEDATE + " TEXT not null," +
                ORIGINALTITLE + " TEXT not null," +
                ORIGINALLANGUAGE + " TEXT not null," +
                TITLE + " TEXT not null," +
                BACKDROPPATH + " TEXT," +
                ADULT + " BOOLEAN not null," +
                VIDEO + " BOOLEAN not null," +
                VOTECOUNT + " DOUBLE not null," +
                POPULARITY + " DOUBLE not null," +
                VOTEAVERAGE + " DOUBLE not null," +
                TAGLINE + " TEXT" + "," +
                STATUS + " TEXT" + ")";

        public static final String CREATE_TABLE_GENRE = "CREATE TABLE IF NOT EXISTS " + TABLE_GENRE + "" +
                "(" + ID_GENRE + " INTEGER PRIMARY KEY not null," +
                NAME + " TEXT not null)";

        public static final String CREATE_TABLE_GENRE_MOVIE = "CREATE TABLE IF NOT EXISTS " + TABLE_GENRE_MOVIE + "" +
                "(" + ID_GENRE + " INTEGER not null," +
                ID_MOVIE + " INTEGER not null," +
                "PRIMARY KEY (" + ID_GENRE + " , " + ID_MOVIE + "))";
    }


    public static final class REFERENCE {
        public static final String MOVIE = Config.PACKAGE_NAME + "movie_item";
    }

    static final class Config {
        static final String PACKAGE_NAME = "com.codingchallenge.nitra.codingchallenge.";
    }


}
