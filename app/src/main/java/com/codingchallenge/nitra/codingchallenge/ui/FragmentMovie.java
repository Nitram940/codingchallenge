package com.codingchallenge.nitra.codingchallenge.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.codingchallenge.nitra.codingchallenge.model.callback.searchSubscribed;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;


public class FragmentMovie extends FragmentBaseMovie {

    @NonNull
    private static List<FragmentMovie> fragmentArrayAdapter = new ArrayList<>();


    public static void newInstance(int sectionNumber) {

        FragmentMovie fragment;
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);

        if (fragmentArrayAdapter.size() < 4) {
            fragment = new FragmentMovie();
            fragment.setArguments(args);
            fragmentArrayAdapter.add(fragment);
        }
    }

    public static FragmentMovie getInstance(int sectionNumber) {
        return fragmentArrayAdapter.get(sectionNumber);
    }

    @Subscribe
    public void onSearchQuery(@NonNull searchSubscribed searchSubscribed) {
        if (searchSubscribed.getEstate() == 1) {
            string_search = searchSubscribed.getQuery();
            initRefresh();
        }
    }


}
