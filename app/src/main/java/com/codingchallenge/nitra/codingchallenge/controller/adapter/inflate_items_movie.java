package com.codingchallenge.nitra.codingchallenge.controller.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.codingchallenge.nitra.codingchallenge.R;
import com.codingchallenge.nitra.codingchallenge.model.helper.Constants;
import com.codingchallenge.nitra.codingchallenge.model.helper.picture;
import com.codingchallenge.nitra.codingchallenge.model.model_json.Movie;
import com.codingchallenge.nitra.codingchallenge.ui.FullscreenActivityDetailMovie;

import java.util.List;

public class inflate_items_movie {

    private Context context;
    private picture picture_picasso;

    public inflate_items_movie(Context context, picture picture_picasso) {
        this.context = context;
        this.picture_picasso = picture_picasso;
    }


    public void inflate_movie(@NonNull View v, @NonNull final Movie movie, @Nullable List<Movie> genres_movies, String genresTitle) {
        CardView moviesLayout;
        TextView movieTitle;
        TextView data;
        TextView movieDescription;
        TextView rating;
        ImageView poster_path;
        RatingBar ratingBar;
        RelativeLayout container_item;
        View container_genres_item;
        TextView textIsOffline;
        ImageView imgIsOffline;
        TextView genre;
        TextView tagline;
        TextView text_title_genre;
        TextView status;
        RecyclerView recyclerView;

        moviesLayout = v.findViewById(R.id.movies_layout);
        movieTitle = v.findViewById(R.id.title);
        data = v.findViewById(R.id.subtitle);
        movieDescription = v.findViewById(R.id.description);
        rating = v.findViewById(R.id.rating);
        poster_path = v.findViewById(R.id.poster_path);
        ratingBar = v.findViewById(R.id.ratingBar);
        textIsOffline = v.findViewById(R.id.textIsOffline);
        imgIsOffline = v.findViewById(R.id.imgIsOffline);
        container_genres_item = v.findViewById(R.id.container_genres_item);
        text_title_genre = v.findViewById(R.id.text_title_genre);

        //ui
        container_item = v.findViewById(R.id.container_item);
        //ui

        //detail
        status = v.findViewById(R.id.status);
        tagline = v.findViewById(R.id.tagline);
        genre = v.findViewById(R.id.genre);

        //genresview
        recyclerView = v.findViewById(R.id.genre_recycler_view);


        if (genres_movies == null) {
            container_genres_item.setVisibility(View.GONE);
            container_item.setVisibility(View.VISIBLE);
            if (movie.isOffline()) {
                imgIsOffline.setImageResource(android.R.drawable.presence_offline);
                textIsOffline.setText(R.string.offline);
            } else {
                imgIsOffline.setImageResource(android.R.drawable.presence_online);
                textIsOffline.setText(R.string.online);
            }

            movieTitle.setText(movie.getTitle());
            if (movie.getGenreTxt() != null) {
                genre.setText(movie.getGenreTxt());
            }
            data.setText(movie.getReleaseDate());
            movieDescription.setText(movie.getOverview());
            tagline.setText(movie.getTagline());

            if (!movie.getNoData()) {
                if (movie.isDetail() || movie.getInfoItem()) {
                    status.setVisibility(View.VISIBLE);
                    movieDescription.setMaxLines(Integer.MAX_VALUE);
                    movieTitle.setEllipsize(null);
                    movieTitle.setMaxLines(Integer.MAX_VALUE);
                    movieDescription.setEllipsize(null);
                    tagline.setVisibility(View.VISIBLE);
                    genre.setVisibility(View.VISIBLE);
                    status.setText(movie.getStatus());
                } else {
                    moviesLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            context.startActivity(
                                    new Intent(context, FullscreenActivityDetailMovie.class)
                                            .putExtra(FullscreenActivityDetailMovie.ID_MOVIE, movie.getId())
                                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        }
                    });
                    movieDescription.setEllipsize(TextUtils.TruncateAt.END);
                    movieDescription.setMaxLines(1);
                    tagline.setVisibility(View.GONE);
                    genre.setVisibility(View.GONE);
                    status.setVisibility(View.GONE);
                    movieTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                    movieTitle.setMaxLines(1);
                }

                if (movie.getVoteAverage() != null) {
                    rating.setText(String.valueOf(movie.getVoteAverage()));
                    ratingBar.setRating(movie.getVoteAverage().floatValue() * 5 / 10);
                }


                if (movie.isDetail()) {

                    if (movie.getPosterPath() != null)
                        picture_picasso.LoadPicasso(Constants.HTTP.BASE_URL_IMAGE_W300 + movie.getBackdropPath(), poster_path, 1,false);
                } else {

                    if (movie.getPosterPath() != null)
                        picture_picasso.LoadPicasso(Constants.HTTP.BASE_URL_IMAGE_W92 + movie.getPosterPath(), poster_path, 1,true);
                }
            }
        } else {
            container_genres_item.setVisibility(View.VISIBLE);
            container_item.setVisibility(View.GONE);

            text_title_genre.setText(genresTitle);

            recyclerView.setItemViewCacheSize(5); // establecer las vistas en cache
            recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            //recycler.setLayoutManager( new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL));
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setAdapter(new MoviesAdapter(genres_movies, R.layout.list_item_movie, context.getApplicationContext(), picture_picasso, false, null));
        }

    }


}

