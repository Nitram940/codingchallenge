package com.codingchallenge.nitra.codingchallenge.controller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codingchallenge.nitra.codingchallenge.model.helper.picture;
import com.codingchallenge.nitra.codingchallenge.model.model_json.Cast;

import java.util.List;


public class CastAdapter extends RecyclerView.Adapter<CastAdapter.CastViewHolder> {

    private List<Cast> casts;
    private int rowLayout;
    private Context context;
    private picture picture_picasso;

    private inflate_items_cast inflateItems;
    private int lastPosition = -1;

    public CastAdapter(List<Cast> casts, int rowLayout, Context context, picture picture_picasso) {
        this.casts = casts;
        this.rowLayout = rowLayout;
        this.context = context;
        this.picture_picasso = picture_picasso;
        this.inflateItems = new inflate_items_cast(context, picture_picasso);
    }


    public void setSearchResponse(List<Cast> castList) {
        this.casts = castList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CastViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                             int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new CastViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull CastViewHolder holder, final int position) {
        inflateItems.inflate_cast(holder.itemView, casts.get(position));

        lastPosition = animViews.setAnimation(holder.itemView, position, lastPosition);
    }

    @Override
    public int getItemCount() {
        if (casts == null) {
            return 0;
        } else {
            return casts.size();
        }

    }

    static class CastViewHolder extends RecyclerView.ViewHolder {

        CastViewHolder(@NonNull View v) {
            super(v);
        }
    }
}