package com.codingchallenge.nitra.codingchallenge.di;

import android.support.annotation.NonNull;

import com.codingchallenge.nitra.codingchallenge.model.callback.MovieApisImpl;
import com.codingchallenge.nitra.codingchallenge.model.callback.MovieService;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MovieModule {

    @NonNull
    @Binds
    public abstract MovieService providesMovieService(MovieApisImpl movieApis);
}
