package com.codingchallenge.nitra.codingchallenge.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.codingchallenge.nitra.codingchallenge.R;
import com.codingchallenge.nitra.codingchallenge.controller.adapter.CastAdapter;
import com.codingchallenge.nitra.codingchallenge.controller.adapter.inflate_items_movie;
import com.codingchallenge.nitra.codingchallenge.di.DaggerApplication;
import com.codingchallenge.nitra.codingchallenge.model.callback.MovieFetchListener;
import com.codingchallenge.nitra.codingchallenge.model.callback.MovieService;
import com.codingchallenge.nitra.codingchallenge.model.database.MoviesDatabase;
import com.codingchallenge.nitra.codingchallenge.model.helper.NetworkCheckValidation;
import com.codingchallenge.nitra.codingchallenge.model.helper.Utils;
import com.codingchallenge.nitra.codingchallenge.model.helper.picture;
import com.codingchallenge.nitra.codingchallenge.model.model_json.Cast;
import com.codingchallenge.nitra.codingchallenge.model.model_json.CreditsResponse;
import com.codingchallenge.nitra.codingchallenge.model.model_json.Movie;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivityDetailMovie extends AppCompatActivity implements MovieFetchListener, NetworkCheckValidation {

    private static final String TAG = FullscreenActivityDetailMovie.class.getSimpleName();
    private static final int UI_ANIMATION_DELAY = 300;
    @NonNull
    public static String ID_MOVIE = "ID_MOVIE";
    private final Handler mHideHandler = new Handler();
    @Nullable
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }

        }
    };
    public int idMovie;
    @Inject
    MoviesDatabase mDatabase;
    @Inject
    MovieService movieService;
    @Inject
    picture picture;
    @Inject
    Utils utils;
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen_detail_movie);

        ((DaggerApplication) getApplication()).getAppComponent().inject(this);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mVisible = true;
        mContentView = findViewById(R.id.fullscreen_content);

        mContentView.findViewById(R.id.container_item).setVisibility(View.GONE);


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        };

        mContentView.findViewById(R.id.movies_layout).setOnClickListener(listener);
        mContentView.findViewById(R.id.cast_recycler_view).setOnClickListener(listener);

        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(listener);

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.

        loadDetail();

        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void loadDetail() {
        idMovie = getIntent().getIntExtra(ID_MOVIE, 0);
        persistentMovies();
        utils.isNetworkAvailable(this);
    }

    private void persistentMovies() {
        mDatabase.fetchMovie(this, idMovie);
    }

    public void readWebService() {

        Observable<Movie> observable = movieService.getMovieDetails(idMovie);

        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Movie>() {
                    @Override
                    public void onCompleted() {
                        Log.e(TAG, "complete");
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e(TAG, e.getMessage());
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(@NonNull Movie movie) {
                        mDatabase.addMovie(movie);
                        inflarDetailView(movie);
                    }
                });

        Observable<CreditsResponse> observable_cast = movieService.getMovieCredits(idMovie);

        observable_cast.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CreditsResponse>() {
                    @Override
                    public void onCompleted() {
                        Log.e(TAG, "complete");
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e(TAG, e.getMessage());
                    }

                    @Override
                    public void onNext(@NonNull CreditsResponse creditsResponse) {
                        //List<Movie> movies = moviesResponse.getResults();
                        mDatabase.addListCasts(creditsResponse.getCast(), idMovie);
                        inflarReciclerviewCast(creditsResponse.getCast());
                    }
                });
    }

    public void inflarReciclerviewCast(List<Cast> casts) {
        RecyclerView recycler = findViewById(R.id.cast_recycler_view);
        recycler.setItemViewCacheSize(5); // establecer las vistas en cache
        recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        //recycler.setLayoutManager( new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL));
        recycler.setNestedScrollingEnabled(false);
        recycler.setAdapter(new CastAdapter(casts, R.layout.list_item_cast, this.getApplicationContext(), picture));
    }

    public void inflarDetailView(@NonNull Movie movie) {
        movie.setDetail(true);
        inflate_items_movie inflate_items_movie=new inflate_items_movie(this,picture);
        inflate_items_movie.inflate_movie(mContentView, movie,null,null);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID_MOVIE represents the Home or Up button.
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    @Override
    public void onDeliverAllMovie(List<Movie> movieItems, int pages) {

    }

    @Override
    public void onDeliverAllCast(List<Cast> CastItems) {
        inflarReciclerviewCast(CastItems);
    }

    @Override
    public void onDeliverMovie(@NonNull Movie movie_item) {
        inflarDetailView(movie_item);
    }

    @Override
    public void onHideDialog() {

    }

    @Override
    public void onNetworkCheckValidation(Boolean bool) {
        if(bool){
            readWebService();
        }
    }
}
