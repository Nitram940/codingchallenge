package com.codingchallenge.nitra.codingchallenge.model.callback;

import android.support.annotation.NonNull;
import android.util.Log;

import com.codingchallenge.nitra.codingchallenge.model.database.MoviesDatabase;
import com.codingchallenge.nitra.codingchallenge.model.helper.Constants;
import com.codingchallenge.nitra.codingchallenge.model.helper.ListGenres;
import com.codingchallenge.nitra.codingchallenge.model.model_json.CreditsResponse;
import com.codingchallenge.nitra.codingchallenge.model.model_json.GenreResponse;
import com.codingchallenge.nitra.codingchallenge.model.model_json.Movie;
import com.codingchallenge.nitra.codingchallenge.model.model_json.MoviesResponse;

import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@Singleton
public class MovieApisImpl implements MovieService {

    private final MovieApis movieApis;

    public static String LANGUAGE_API="en";

    private MoviesDatabase mDatabase;
    private ListGenres listGenres;

    @Inject
    MovieApisImpl(MovieApis movieApis, MoviesDatabase mDatabase,ListGenres listGenres) {
        this.movieApis = movieApis;
        this.mDatabase=mDatabase;
        this.listGenres=listGenres;

        LANGUAGE_API= Locale.getDefault().toLanguageTag();
    }


    @NonNull
    @Override
    public Observable<MoviesResponse> getTopRatedMovies(int page) {
        return movieApis.getTopRatedMovies(Constants.API_KEY, LANGUAGE_API, page);
    }

    @NonNull
    @Override
    public Observable<MoviesResponse> getPopularMovies(int page) {
        return movieApis.getPopularMovies(Constants.API_KEY, LANGUAGE_API, page);
    }

    @NonNull
    @Override
    public Observable<MoviesResponse> getUpcomingMovies(int page) {
        return movieApis.getUpcomingMovies(Constants.API_KEY, LANGUAGE_API, page);
    }

    @NonNull
    @Override
    public Observable<Movie> getMovieDetails(int id) {
        return movieApis.getMovieDetails(id, Constants.API_KEY, LANGUAGE_API);
    }

    @NonNull
    @Override
    public Observable<CreditsResponse> getMovieCredits(int movie_id) {
        return movieApis.getMovieCredits(movie_id, Constants.API_KEY);
    }

    @Override
    public void getMovieGenres() {
        movieApis.getMovieGenres(Constants.API_KEY, LANGUAGE_API).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GenreResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e("getMovieGenres", e.getMessage());
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(@NonNull GenreResponse genreResponse) {
                        mDatabase.addListGenre(genreResponse.getGenres());
                        listGenres.setGenreNameId(genreResponse.getGenres());
                    }
                });
    }

    @NonNull
    @Override
    public Observable<GenreResponse> getTvGenres() {
        return movieApis.getTvGenres(Constants.API_KEY, LANGUAGE_API);
    }

    @NonNull
    @Override
    public Observable<MoviesResponse> getMovieSearch(int page, String query) {
        return movieApis.getMovieSearch(query, Constants.API_KEY, LANGUAGE_API, page);
    }
}
