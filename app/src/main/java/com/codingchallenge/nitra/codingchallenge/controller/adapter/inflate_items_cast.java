package com.codingchallenge.nitra.codingchallenge.controller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.codingchallenge.nitra.codingchallenge.R;
import com.codingchallenge.nitra.codingchallenge.model.helper.Constants;
import com.codingchallenge.nitra.codingchallenge.model.helper.picture;
import com.codingchallenge.nitra.codingchallenge.model.model_json.Cast;

import javax.inject.Inject;

import dagger.Module;

@Module
public class inflate_items_cast {

    private Context context;
    private picture picture_picasso;

    @Inject
    inflate_items_cast(Context context, picture picture_picasso) {
        this.context = context;
        this.picture_picasso = picture_picasso;
    }

    void inflate_cast(@NonNull View v, @NonNull final Cast cast) {

        final CardView moviesLayout;
        TextView cast_name;
        TextView cast_character;
        ImageView cast_path;


        cast_name = v.findViewById(R.id.cast_name);
        cast_character = v.findViewById(R.id.cast_character);
        cast_path = v.findViewById(R.id.cast_path);

        cast_name.setText(cast.getName());
        cast_character.setText(cast.getCharacter());

        if (cast.getProfile_path() != null) {
            picture_picasso.LoadPicasso(
                    Constants.HTTP.BASE_URL_IMAGE_W92 + cast.getProfile_path(),
                    cast_path, 1,false);
        } else {
            picture_picasso.LoadPicasso(R.drawable.ic_action_account_circle_40,
                    cast_path);
        }
    }

}

